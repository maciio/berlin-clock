package com.ubs.opsit.interviews.impl;

import com.ubs.opsit.interviews.TimeConverter;

import java.util.Collections;
import java.util.stream.Stream;

/**
 * Created by Mario A. Pineda on 6/29/17.
 */
public class BerlinTimeConverterImpl implements TimeConverter{

    private static final String NEW_LINE = System.getProperty("line.separator");

    public String convertTime(String aTime) {
        if (aTime == null) {
            throw new IllegalArgumentException("No time to process");
        }
        int hours, min, secs = 0;
        try {
            int[] timeArray = Stream.of(aTime.split(":")).mapToInt(Integer::parseInt).toArray();
            if (timeArray.length != 3) {
                throw new IllegalArgumentException("Invalid time format");
            }
            hours = timeArray[0];
            min = timeArray[1];
            secs = timeArray[2];
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Not valid time format", e);
        }
        return process(hours, min, secs);
    }

    public String process(int hours, int min, int secs) {
        String row1 = (secs % 2 == 0) ? "Y" : "O";
        String row2 = getRowFormat(hours / 5, 4, "R");
        String row3 = getRowFormat(hours % 5, 4, "R");
        String row4 = getRowFormat(min / 5, 11, "Y").replaceAll("YYY", "YYR");
        String row5 = getRowFormat(min % 5, 4, "Y");

        return String.join(NEW_LINE, row1, row2, row3, row4, row5);
    }

    public String getRowFormat(int lightsOn, int lightsPerRow, String lightType) {
        int remainLights = lightsPerRow - lightsOn;
        String on = String.join("", Collections.nCopies(lightsOn, lightType));
        String off = String.join("", Collections.nCopies(remainLights, "O"));

        return on.concat(off);
    }

}

