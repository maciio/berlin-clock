package com.ubs.opsit.interviews;

import com.ubs.opsit.interviews.impl.BerlinTimeConverterImpl;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Test;

import static com.ubs.opsit.interviews.support.BehaviouralTestEmbedder.aBehaviouralTestRunner;
import static junit.framework.TestCase.assertTrue;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Acceptance test class that uses the JBehave (Gerkin) syntax for writing stories.  You should not need to
 * edit this class to complete the exercise, this is your definition of done.
 */
public class BerlinClockFixture {

    private static final String NEW_LINE = System.getProperty("line.separator");


    private TimeConverter berlinClock = new BerlinTimeConverterImpl();
    private String theTime;

    @Test
    public void berlinClockAcceptanceTests() throws Exception {
        aBehaviouralTestRunner()
                .usingStepsFrom(this)
                .withStory("berlin-clock.story")
                .withStory("time_process.story")
                .run();
    }

    @When("the time is $time")
    public void whenTheTimeIs(String time) {
        theTime = time;
    }

    @Then("the clock should look like $")
    public void thenTheClockShouldLookLike(String theExpectedBerlinClockOutput) {
        assertThat(berlinClock.convertTime(theTime)).isEqualTo(theExpectedBerlinClockOutput);
    }

    @Then("the clock is wrong $")
    public void thenWrongTheClockShouldLookLike(String theExpectedBerlinClockOutput) {
        try {
            berlinClock.convertTime(theTime);
        } catch (IllegalArgumentException e) {
            assertTrue(e instanceof IllegalArgumentException);
        }
    }

    @Test
    public void testProcess() {
        BerlinTimeConverterImpl testClass = new BerlinTimeConverterImpl();
        String case1 = testClass.process(0, 00, 01);
        String expected1 = "O".concat(NEW_LINE)
                .concat("OOOO").concat(NEW_LINE)
                .concat("OOOO").concat(NEW_LINE)
                .concat("OOOOOOOOOOO").concat(NEW_LINE)
                .concat("OOOO");
        String case2 = testClass.process(23, 59, 59);
        String expected2 = "O".concat(NEW_LINE)
                .concat("RRRR").concat(NEW_LINE)
                .concat("RRRO").concat(NEW_LINE)
                .concat("YYRYYRYYRYY").concat(NEW_LINE)
                .concat("YYYY");
        assertThat(case1).isEqualTo(expected1);
        assertThat(case2).isEqualTo(expected2);
    }

    @Test
    public void testRowFormat() {
        BerlinTimeConverterImpl testClass = new BerlinTimeConverterImpl();
        String expected1 = testClass.getRowFormat(1, 5, "X");
        String expected2 = testClass.getRowFormat(6, 10, "Y");
        String expected3 = testClass.getRowFormat(0, 1, "Z");
        assertThat(expected1).isEqualTo("XOOOO");
        assertThat(expected2).isEqualTo("YYYYYYOOOO");
        assertThat(expected3).isEqualTo("O");
    }
}
