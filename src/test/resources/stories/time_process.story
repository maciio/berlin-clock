Story: The Berlin Clock goes wrong

Meta:
@scope interview

Narrative:
    Just test internal functions

Scenario: Wrong Hour
When the time is 23:33
Then the clock is wrong
0
DOESNT
EVENMATTER
YYR00000000
YY00

Scenario: Wrong Hour
When the time is 12:da:12
Then the clock is wrong
0
RR00
RRR0
YYR00000000
YY00


